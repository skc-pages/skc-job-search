.PHONY: test

BASE_DIR = $(dir $(shell pwd))
DIR = $(notdir $(shell pwd))
ROOT_DIR = ${BASE_DIR}/${DIR}
ADOC_DIR = ${ROOT_DIR}/adoc
PUBLIC_DIR = ${ROOT_DIR}/public
REL9_VIRT_DIR = ${ADOC_DIR}/rel9-virtualization


test:
	echo 'this is a test';
	echo ${BASE_DIR}
	echo ${DIR}
	echo ${ROOT_DIR}
	echo ${ADOC_DIR}

build_all: build_index_adoc build_dell-technologies build_humana

build_index_adoc:
	cd ${ADOC_DIR}; \
	pwd ; \
	asciidoctor *.adoc; \
	ls -lart;

build_dell-technologies:
	cd ${ADOC_DIR}/jobs/may/dell-technologies; \
	pwd; \
	asciidoctor *.adoc; \
	ls -alrt;

build_humana:
	cd ${ADOC_DIR}/jobs/may/humana; \
	pwd; \
	asciidoctor *.adoc; \
	ls -alrt;


deploy_public:
	cd ${ADOC_DIR}; \
	pwd ; \
	cp -R . ${PUBLIC_DIR}; \
	find ${PUBLIC_DIR} -type f ! -name '*.html' -delete; 

